class InitialSchema < ActiveRecord::Migration

  def self.up

    #
    # UserProfile
    #
    # A logged in internale type user with permissions - not regular sponsors of families
    #
    create_table :user_profile do |t|
      t.string	:name,  limit: 100, null: false
      t.string  :phone, limit: 20
      t.boolean	:active, null: false, default: true
      t.string  :permissions, array: true, default: []
      
      ## Database authenticatable
      t.string  :email, limit: 250,  null: false
      t.string  :encrypted_password, null: false, default: ""
      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at
      ## Rememberable
      t.datetime :remember_created_at
      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip
      ## Lockable
      t.integer  :failed_attempts, default: 0, null: false          # Only if lock strategy is :failed_attempts
      t.string   :unlock_token                                      # Only if unlock strategy is :email or :both
      t.datetime :locked_at
      ## Confirmable -- NOT USED
      # t.string   :confirmation_token
      # t.datetime :confirmed_at
      # t.datetime :confirmation_sent_at
      # t.string   :unconfirmed_email # Only if using reconfirmable

      t.timestamps
      
      t.references :created_by, :class_name => 'UserProfile', :foreign_key => true
      t.references :updated_by, :class_name => 'UserProfile', :foreign_key => true

      t.index :email, unique: true
      t.index :reset_password_token, unique: true
    end



    #
    # Program
    #
    # The specific volunteer center activity, e.g. "Share Your Christmas" or "Backpacks for School"
    #   has_many :drives
    #
    #   program.current_drive
    #
    create_table :program do |t|
      t.string :name, limit: 100, null: false
      t.string :season                                        # Christmas, Thanksgiving, Spring
      t.text :about
      t.boolean :active, default: true, null: false

      t.audit_cols
      
      t.index :name
    end


    #
    # Drive
    #
    # Programs often repeat each year, so familes and clients will be associated with the specific Drive
    #   belongs_to :program
    #   has_many :organizations, through: :partnership
    #   has_many :deadlines
    #
    #   Drive.first.organizations
    #
    create_table :drive do |t|
      t.references :program, null: false
      t.string :name, limit: 50, null: false                 # e.g. "2012", or "Winter 2012"
      t.boolean :active, default: true, null: false
      
      t.audit_cols
      
      t.foreign_key :program
      t.index :program_id
      t.index :name
    end


    #
    # Deadline
    #
    # Dates associated with a Drive
    #
    #   belongs_to :drive
    #
    create_table :deadline do |t|
      t.references :drive, null: false
      t.string :name, limit: 60, null: false
      t.date :due_date, null: false
      t.time :due_time
      t.date :reminder_date
      t.string :description, limit: 100, null: false
      
      t.audit_cols
      
      t.foreign_key :drive
      t.index :drive_id
    end



    #
    # Organization
    #
    # The collaborating organization - e.g. "Aldersgate United Methodist Church"
    #   has_many :drives, through: :partnership
    #   belongs_to :leader (UserProfile)
    #
    create_table :organization do |t|
      t.string :name,  limit: 100, null: false
      t.string :key, limit: 50                                       # short name for URL
      t.string :email_from, limit: 100                                      # the visual from in email if possible
      t.string :image_uid, :limit => 500
      t.string :image_name, :limit => 100
      t.boolean :active, default: true, null: false

      t.audit_cols
      
      t.index :name
      t.index :key
    end


    #
    # Partnership
    #
    # the association between a program drive and partnering organization
    # Indicates a Organization's participation in a Drive (may not be necessary, if only to filter participating orgs in a drive)
    #
    create_table :partnership do |t|
      t.references :drive, null: false
      t.references :organization, null: false
      t.references :leader, null: false, class_name: 'UserProfile'
      t.text :instructions
      t.text :about
      t.boolean :active, default: true, null: false

      t.audit_cols
      
      t.foreign_key :drive
      t.foreign_key :organization
      t.foreign_key :user_profile, column: 'leader_id'
      t.index [:drive_id, :organization_id]
      t.index :leader_id
    end


    #
    # SocialWorker
    #
    # The Social Worker assiting the client family
    #   has_many :clients
    #
    create_table :social_worker do |t|
      t.string :name,  limit: 100, null: false
      t.string :email, limit: 200
      t.string :phone, limit: 20
      t.boolean :active, default: true, null: false

      t.audit_cols
      
      t.index :name
    end


    #
    # Client
    #
    # The Family registering for assistance - associated with a partnership (Drive + Organization)
    # Notes:
    #   - a client can belong to a drive, before being assigned to an organization (through a partrnership) (Volunteer Center enters/imports clients for a year/drive)
    #   - a client is then assigned to a partnership
    #
    #   belongs_to :drive
    #   belongs_to :partnership
    #   belongs_to :social_worker
    #
    #   has_many :family_members
    #   has_one :sponsorship          (for food)
    #
    # ===============> consider no drive_id and requiring partnership
    # ===============> then, every drive would have to have a partnership into which clients can be added - perhaps a default partnership (Holding, or something)
    # 
    create_table :client do |t|
      t.references :drive,    null: false
      t.references :partnership,  null: true                  # until assigned
      t.references :social_worker, null: true                 # may not be available at first

      t.integer :casenumber                                   # SYC# needs to be integer for proper sorting
      t.string :last_name,  limit: 40, null: false            # Patterson-Robertson, III
      t.string :first_name, limit: 40                         # Theodore Alexander Frederick
      t.string :address,    limit: 200
      t.string :city,       limit: 50
      t.string :state,      limit: 2, default: 'NC'
      t.string :zip,        limit: 10
      t.string :phone,      limit: 100
      t.string :profile,    limit: 2000
      
      t.boolean :deliverable, null: false, default: true
      t.string  :latlng, limit: 60

      t.audit_cols
      
      t.foreign_key :drive
      t.foreign_key :partnership
      t.foreign_key :social_worker
      t.index :drive_id
      t.index :partnership_id
      t.index :social_worker_id
      t.index :casenumber
    end


    # FamilyMember
    #
    # members of the client family
    #   belongs_to :client
    #   has_one :sponsorship
    #
    create_table :family_member do |t|
      t.references :client, null: false

      t.string  :first_name, limit: 60, null: false           # generally, we don't collect full names
      t.decimal :age, :precision => 5, :scale => 2            # in years, up to 199.99 (express months as decimal years 1/12, 2/12, etc) - gets rounded in display
      t.string  :gender, limit: 1                             # class Gender::OPTIONS = {"F"=>"Female", "M"=>"Male", "U"=>"Unknown"}
      t.string  :race, limit: 1                               # class Race::OPTIONS = {"A"=>"Asian", "B"=>"Black", "H"=>"Hispanic", "W"=>"White"}
      t.string  :size                                         # text: Infant, Toddler, Petite, Small, Medium, Large, X-Large, XX-Large, XXX-Large
      t.string  :need, limit: 1000                            # gift request
      t.boolean :bike, null: false, default: false            # special flag if gift requests include a Bicycle

      t.audit_cols
      
      t.foreign_key :client
      t.index [:client_id, :age]
    end


    ##############################################################
    # created through public access so don't include audit columns
    ##############################################################

    #
    # Sponsor
    #
    # The person sponsoring a family member. Maybe we'll add devise_to this model later
    #   has_many :family_members
    #
    create_table :sponsor do |t|
      t.string :name,  limit: 100, null: false
      t.string :email, limit: 200, null: false
      t.string :phone, limit: 20

      ### no audit columns because it is posted un-authenticated 
      ### t.audit_cols
      t.timestamps
      
      t.index :email
    end


    # Sponsorship
    #
    #   belongs_to :sponsor
    #   belongs_to :client
    #   belongs_to :family_member
    #
    create_table :sponsorship do |t|
      t.references :sponsor, null: false
      t.references :client, null: true
      t.references :family_member, null: true

      ### no audit columns because it is posted un-authenticated 
      ### t.audit_cols
      t.timestamps
      
      t.foreign_key :sponsor
      t.foreign_key :client
      t.foreign_key :family_member
      t.index :sponsor_id
      t.index :client_id
      t.index :family_member_id
    end


    #
    # Notification       ========================== work in progress =================
    # 
    create_table :notification do |t|
      t.references :drive, null: false
      t.references :deadline, null: true                            # think about this
      t.references :sponsor, null: false
      t.datetime :datetime, null: false
    end

  end

  def self.down
    # nop
  end

end
