class AddHomePageToPartnership < ActiveRecord::Migration

  def self.up
    add_column :partnership, :home_page, :text
  end

  def self.down
    remove_column :partnership, :home_page
  end

end
