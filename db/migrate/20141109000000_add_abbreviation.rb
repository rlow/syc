class AddAbbreviation < ActiveRecord::Migration

  def self.up
    add_column :program, :abbreviation, :string
    add_column :organization, :abbreviation, :string
  end

  def self.down
    remove_column :program, :abbreviation
    remove_column :organization, :abbreviation
  end

end
