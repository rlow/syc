class AddVerifiedToClient < ActiveRecord::Migration

  def self.up
    add_column :client, :verified, :boolean, null: false, default: false
  end

  def self.down
    remove_column :client, :verified
  end

end
