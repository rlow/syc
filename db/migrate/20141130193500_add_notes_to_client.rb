class AddNotesToClient < ActiveRecord::Migration

  def self.up
    add_column :client, :notes, :text
  end

  def self.down
    remove_column :client, :notes
  end

end
