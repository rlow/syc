# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141202170000) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "client", force: true do |t|
    t.integer  "drive_id",                                      null: false
    t.integer  "partnership_id"
    t.integer  "social_worker_id"
    t.integer  "casenumber"
    t.string   "last_name",        limit: 40,                   null: false
    t.string   "first_name",       limit: 40
    t.string   "address",          limit: 200
    t.string   "city",             limit: 50
    t.string   "state",            limit: 2,    default: "NC"
    t.string   "zip",              limit: 10
    t.string   "phone",            limit: 100
    t.string   "profile",          limit: 2000
    t.boolean  "deliverable",                   default: true,  null: false
    t.string   "latlng",           limit: 60
    t.integer  "created_by_id",                                 null: false
    t.integer  "updated_by_id",                                 null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "notes"
    t.boolean  "verified",                      default: false, null: false
  end

  add_index "client", ["casenumber"], name: "index_client_on_casenumber", using: :btree
  add_index "client", ["drive_id"], name: "index_client_on_drive_id", using: :btree
  add_index "client", ["partnership_id"], name: "index_client_on_partnership_id", using: :btree
  add_index "client", ["social_worker_id"], name: "index_client_on_social_worker_id", using: :btree

  create_table "deadline", force: true do |t|
    t.integer  "drive_id",                  null: false
    t.string   "name",          limit: 60,  null: false
    t.date     "due_date",                  null: false
    t.time     "due_time"
    t.date     "reminder_date"
    t.string   "description",   limit: 100, null: false
    t.integer  "created_by_id",             null: false
    t.integer  "updated_by_id",             null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "deadline", ["drive_id"], name: "index_deadline_on_drive_id", using: :btree

  create_table "drive", force: true do |t|
    t.integer  "program_id",                              null: false
    t.string   "name",          limit: 50,                null: false
    t.boolean  "active",                   default: true, null: false
    t.integer  "created_by_id",                           null: false
    t.integer  "updated_by_id",                           null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "drive", ["name"], name: "index_drive_on_name", using: :btree
  add_index "drive", ["program_id"], name: "index_drive_on_program_id", using: :btree

  create_table "family_member", force: true do |t|
    t.integer  "client_id",                                                          null: false
    t.string   "first_name",    limit: 60,                                           null: false
    t.decimal  "age",                        precision: 5, scale: 2
    t.string   "gender",        limit: 1
    t.string   "race",          limit: 1
    t.string   "size"
    t.string   "need",          limit: 1000
    t.boolean  "bike",                                               default: false, null: false
    t.integer  "created_by_id",                                                      null: false
    t.integer  "updated_by_id",                                                      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "family_member", ["client_id", "age"], name: "index_family_member_on_client_id_and_age", using: :btree

  create_table "notification", force: true do |t|
    t.integer  "drive_id",    null: false
    t.integer  "deadline_id"
    t.integer  "sponsor_id",  null: false
    t.datetime "datetime",    null: false
  end

  create_table "organization", force: true do |t|
    t.string   "name",          limit: 100,                null: false
    t.string   "key",           limit: 50
    t.string   "email_from",    limit: 100
    t.string   "image_uid",     limit: 500
    t.string   "image_name",    limit: 100
    t.boolean  "active",                    default: true, null: false
    t.integer  "created_by_id",                            null: false
    t.integer  "updated_by_id",                            null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "abbreviation"
  end

  add_index "organization", ["key"], name: "index_organization_on_key", using: :btree
  add_index "organization", ["name"], name: "index_organization_on_name", using: :btree

  create_table "partnership", force: true do |t|
    t.integer  "drive_id",                       null: false
    t.integer  "organization_id",                null: false
    t.integer  "leader_id",                      null: false
    t.text     "instructions"
    t.text     "about"
    t.boolean  "active",          default: true, null: false
    t.integer  "created_by_id",                  null: false
    t.integer  "updated_by_id",                  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "home_page"
  end

  add_index "partnership", ["drive_id", "organization_id"], name: "index_partnership_on_drive_id_and_organization_id", using: :btree
  add_index "partnership", ["leader_id"], name: "index_partnership_on_leader_id", using: :btree

  create_table "program", force: true do |t|
    t.string   "name",          limit: 100,                null: false
    t.string   "season"
    t.text     "about"
    t.boolean  "active",                    default: true, null: false
    t.integer  "created_by_id",                            null: false
    t.integer  "updated_by_id",                            null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "abbreviation"
  end

  add_index "program", ["name"], name: "index_program_on_name", using: :btree

  create_table "social_worker", force: true do |t|
    t.string   "name",          limit: 100,                null: false
    t.string   "email",         limit: 200
    t.string   "phone",         limit: 20
    t.boolean  "active",                    default: true, null: false
    t.integer  "created_by_id",                            null: false
    t.integer  "updated_by_id",                            null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "social_worker", ["name"], name: "index_social_worker_on_name", using: :btree

  create_table "sponsor", force: true do |t|
    t.string   "name",       limit: 100, null: false
    t.string   "email",      limit: 200, null: false
    t.string   "phone",      limit: 20
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sponsor", ["email"], name: "index_sponsor_on_email", using: :btree

  create_table "sponsorship", force: true do |t|
    t.integer  "sponsor_id",       null: false
    t.integer  "client_id"
    t.integer  "family_member_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sponsorship", ["client_id"], name: "index_sponsorship_on_client_id", using: :btree
  add_index "sponsorship", ["family_member_id"], name: "index_sponsorship_on_family_member_id", using: :btree
  add_index "sponsorship", ["sponsor_id"], name: "index_sponsorship_on_sponsor_id", using: :btree

  create_table "user_profile", force: true do |t|
    t.string   "name",                   limit: 100,                null: false
    t.string   "phone",                  limit: 20
    t.boolean  "active",                             default: true, null: false
    t.string   "permissions",                        default: [],                array: true
    t.string   "email",                  limit: 250,                null: false
    t.string   "encrypted_password",                 default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",                    default: 0,    null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "created_by_id"
    t.integer  "updated_by_id"
  end

  add_index "user_profile", ["email"], name: "index_user_profile_on_email", unique: true, using: :btree
  add_index "user_profile", ["reset_password_token"], name: "index_user_profile_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "client", "drive", name: "client_drive_id_fk"
  add_foreign_key "client", "partnership", name: "client_partnership_id_fk"
  add_foreign_key "client", "social_worker", name: "client_social_worker_id_fk"
  add_foreign_key "client", "user_profile", name: "client_cr_by_fk", column: "created_by_id"
  add_foreign_key "client", "user_profile", name: "client_ub_by_fk", column: "updated_by_id"

  add_foreign_key "deadline", "drive", name: "deadline_drive_id_fk"
  add_foreign_key "deadline", "user_profile", name: "deadline_cr_by_fk", column: "created_by_id"
  add_foreign_key "deadline", "user_profile", name: "deadline_ub_by_fk", column: "updated_by_id"

  add_foreign_key "drive", "program", name: "drive_program_id_fk"
  add_foreign_key "drive", "user_profile", name: "drive_cr_by_fk", column: "created_by_id"
  add_foreign_key "drive", "user_profile", name: "drive_ub_by_fk", column: "updated_by_id"

  add_foreign_key "family_member", "client", name: "family_member_client_id_fk"
  add_foreign_key "family_member", "user_profile", name: "family_member_cr_by_fk", column: "created_by_id"
  add_foreign_key "family_member", "user_profile", name: "family_member_ub_by_fk", column: "updated_by_id"

  add_foreign_key "organization", "user_profile", name: "organization_cr_by_fk", column: "created_by_id"
  add_foreign_key "organization", "user_profile", name: "organization_ub_by_fk", column: "updated_by_id"

  add_foreign_key "partnership", "drive", name: "partnership_drive_id_fk"
  add_foreign_key "partnership", "organization", name: "partnership_organization_id_fk"
  add_foreign_key "partnership", "user_profile", name: "partnership_cr_by_fk", column: "created_by_id"
  add_foreign_key "partnership", "user_profile", name: "partnership_leader_id_fk", column: "leader_id"
  add_foreign_key "partnership", "user_profile", name: "partnership_ub_by_fk", column: "updated_by_id"

  add_foreign_key "program", "user_profile", name: "program_cr_by_fk", column: "created_by_id"
  add_foreign_key "program", "user_profile", name: "program_ub_by_fk", column: "updated_by_id"

  add_foreign_key "social_worker", "user_profile", name: "social_worker_cr_by_fk", column: "created_by_id"
  add_foreign_key "social_worker", "user_profile", name: "social_worker_ub_by_fk", column: "updated_by_id"

  add_foreign_key "sponsorship", "client", name: "sponsorship_client_id_fk"
  add_foreign_key "sponsorship", "family_member", name: "sponsorship_family_member_id_fk"
  add_foreign_key "sponsorship", "sponsor", name: "sponsorship_sponsor_id_fk"

end
