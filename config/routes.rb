Vc::Application.routes.draw do

  devise_for :user_profiles,
      :path => '/account',
      :path_names => { :sign_in => 'sign_in', :sign_out => 'sign_out' },
      :skip => [:registrations]

  as :user_profile do
    get 'account/edit' => 'devise/registrations#edit', :as => 'edit_user_profile_registration'
    put 'account' => 'devise/registrations#update', :as => 'user_profile_registration'
  end

  authenticated :user_profile do
    root :to => 'programs#index', as: :authenticated_root
  end

  unauthenticated :user_profile do
    root :to => "public/partner#index", as: :unauthenticated_root
  end

  resources :clients, :deadlines, :drives, :family_members, :organizations, :partnerships,
            :programs, :social_workers, :sponsors, :sponsorships, :user_profiles

  get '/clients/:id/view(.:format)', to: 'clients#view', as: 'view_client'

  get 'login', to: 'programs#index', as: :login
  get 'admin', to: 'programs#index', as: :admin


  scope '/partner' do
    get '/:key/:partnership_id/about',         to: 'public/partner#about',        as: :partner_about
    get '/:key/:partnership_id/instructions',  to: 'public/partner#instructions', as: :partner_instructions
    get '/:key/:partnership_id/clients',       to: 'public/partner#clients',      as: :partner_clients
    get '/:key/:partnership_id/clients/:id',   to: 'public/partner#client',       as: :partner_client
    get '/:key/:partnership_id/members',       to: 'public/partner#members',      as: :partner_members

    get '/:key/:partnership_id/bigboard/(:option)',         to: 'public/partner#bigboard',    as: :partner_bigboard

    get    '/:key/:partnership_id/sponsor/:what/:id/new',   to: 'public/sponsorship#new',     as: :partner_new_sponsor
    post   '/:key/:partnership_id/sponsor',                 to: 'public/sponsorship#create',  as: :partner_create_sponsor
    put    '/:key/:partnership_id/sponsor',                 to: 'public/sponsorship#create',  as: :partner_put_sponsor
    patch  '/:key/:partnership_id/sponsor',                 to: 'public/sponsorship#create',  as: :partner_patch_sponsor
    get    '/:key/:partnership_id/thanks/:id/:sponsor_id',  to: 'public/partner#thanks',      as: :partner_thanks
    ## no edit --> get  '/:key/:partnership_id/sponsor/:what/:id/edit',  to: 'public/sponsorship#edit',    as: :partner_edit_sponsor

    get '/:key/:partnership_id',               to: 'public/partner#home',      as: :partner_home
    get '/:key',                               to: 'public/partner#index',     as: :partner_index
    get '/',                                   to: 'public/partner#welcome',   as: :partner_welcome
  end


end
