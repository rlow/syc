class SponsorMailer < ActionMailer::Base

  default from: 'Share Your Christmas'

  layout 'mailer'

  # SponsorMailer.thanks(Organization.find(1), Partnership.find(2), Client.find(7), Sponsor.find(1)).deliver

  def thanks(organization, partnership, client, sponsor)
    @organization = organization
    @partnership = partnership
    @client = client
    @sponsor = sponsor

    mail(to: @sponsor.email, subject: 'Share Your Christmas Sponsorship')
  end


  # syntax: SponsorMailer.reminder(@partnership, @sponsor).deliver
  # test: SponsorMailer.reminder(Partnership.find(2), Sponsor.find(2)).deliver
  #
  # to run for all:
  #
  #  ssh -g -L 5432:localhost:5432 topsail@shareyourchristmas.net
  #  bundle exec rails c production
  #  partnership = Partnership.last
  #  Sponsor.for_this_year.each{|sponsor| SponsorMailer.reminder(partnership, sponsor).deliver; sleep(2)};nil
  #
  # Sponsor.for_partnership(p).each     TODO:  ==> need to write that scope
  #
  def reminder(partnership, sponsor)
    @partnership = partnership
    @sponsor = sponsor
    @family_members = FamilyMember.for_partnership(partnership).for_sponsor(sponsor)
    @clients = Client.for_partnership(partnership).for_sponsor(sponsor)
    # TO TEST change to: to my email address
    to = @sponsor.email                              #  "cat.penny.lane@gmail.com"
    if @family_members.present? || @clients.present?
      mail( to: to,
            subject: 'Share Your Christmas Reminder'
          )
    end
  end


  # syntax: SponsorMailer.appeal(@partnership, @sponsor).deliver
  # test:  SponsorMailer.appeal(Partnership.find(4), Sponsor.find(2)).deliver
  #
  # to run for all:
  #
  #  ssh -g -L 5432:localhost:5432 topsail@shareyourchristmas.net
  #  bundle exec rails c production
  #  partnership = Partnership.find(4)
  #  sponsors_to_email = Sponsor.all.reject{|sponsor| sponsor.sponsorships.created_at_on_or_after(Date.parse('2015-11-21')).any? }
  #  sponsors_to_email.each{|sponsor| SponsorMailer.appeal(partnership, sponsor).deliver; sleep(3)};nil
  #
  # Sponsor.for_partnership(p).each     TODO:  ==> need to write that scope
  #

  def appeal(partnership, sponsor)
    @partnership = partnership
    @sponsor = sponsor
    # TO TEST change to: to my email address
    to = @sponsor.email                               #  "cat.penny.lane@gmail.com"
    mail( to: to,
          bcc: "low00001@gmail.com",
          subject: 'Share Your Christmas Request'
        )
  end

  # partnership = Partnership.find(4)
  # sponsors_to_email = Sponsor.for_this_year(false).reject{|s| Sponsor.for_this_year.all.map(&:name).include?(s.name)}
  # sponsors_to_email.each{|sponsor| SponsorMailer.plea(partnership, sponsor).deliver; sleep(3)};nil
  #
  def plea(partnership, sponsor)
    @partnership = partnership
    @sponsor = sponsor
    to = @sponsor.email                               #  "cat.penny.lane@gmail.com"
    mail( to: to,
          subject: 'Need your Share Your Christmas help'
        )
  end

end