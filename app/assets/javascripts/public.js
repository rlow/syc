// limited javascript required only by the public pages, specificaly excluding:
//   topsail_starter
//   require_tree
// the require directive slurps in a file only once even if it appears multiple times, 
// so it's ok if this file gets slurped in by the application require_tree . directive
//  MUST make sure this file is included in production.rb precompiled assets
//
//= require jquery
//= require jquery_ujs