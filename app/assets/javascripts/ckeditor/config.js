/*
CKEDITOR.editorConfig = function(config) {
   config.language = 'en';
   config.width = '100%';
   config.height = '400';
   config.format_tags = 'h1;h2;h3;div;p';
   
   config.toolbar_Pure = [
      {
        name: 'styles',
        items: ['Format','Font','FontSize']
      },
      {
        name: 'basicstyles',
        items: ['Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ]
      },
      {
        name: 'colors',      
        items : [ 'TextColor','BGColor' ] 
      },
      {
        name: 'links',
        items: ['Link', 'Unlink']
      },
      {
        name: 'paragraph',
        items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
      },
      {
        name: 'clipboard', 
        items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ]
      },
      {
        name: 'insert',
        items: ['Image', 'Iframe', 'Table', 'SpecialChar', 'HorizontalRule']
      },
      {
        name: 'document',
        items : [ 'Source']
      }
    ];
   
   config.toolbar = 'Pure';
   return true;
};
*/