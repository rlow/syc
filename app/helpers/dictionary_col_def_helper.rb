# column definition for the different dictionary model tables
module DictionaryColDefHelper

  def index_col_def(relation_or_instance)
    model_class = relation_or_instance.is_a?(ActiveRecord::Base) ? relation_or_instance.class : relation_or_instance.model
    filter = relation_or_instance.is_a?(ActiveRecord::Base) ? {} : relation_or_instance.filter
    case model_class.name

    when 'Client'
      {
        program:      ->(r){ r.drive.andand.program.andand.name },
        drive:        ->(r){ r.drive.andand.name },
        casenumber:   ->(r){ r.casenumber },
        name:         ->(r){ r.name },
        address:      ->(r){ r.address },
        city:         ->(r){ r.city },
        zip:          ->(r){ r.zip },
        members:      ->(r){ r.family_members.count },
        verified:     ->(r){ r.verified ? 'Yes' : ''},
        ' ' => ->(r){ link_to 'View', view_client_path(r), remote: false, class: 'edit' },
        ''  => ->(r){ link_to 'Edit', edit_client_path(r), remote: true, class: 'edit' if can?(:edit, r)}
      }

    when 'Drive'
      {
        program:      ->(r){ r.program.name },
        drive_name:   ->(r){ r.name },
        active:       ->(r){ yes_no(r.active) },
        '' => ->(r){ link_to 'Edit', edit_drive_path(r), remote: true, class: 'edit' if can?(:edit, r)}
      }

    when 'Deadline'
      {
        drive:          ->(r){ r.drive.name },
        name:           ->(r){ r.name },
        due_date:       ->(r){ r.due_date },
        due_time:       ->(r){ r.due_time },
        reminder_date:  ->(r){ r.reminder_date },
        ''  => ->(r){ link_to 'Edit', edit_deadline_path(r), remote: true, class: 'edit' if can?(:edit, r)}
      }

    when 'FamilyMember'
      {
        syc:      ->(r){ r.client.casenumber },
        name:     ->(r){ r.first_name },
        age:      ->(r){ r.age_string },
        gender:   ->(r){ r.gender },
        race:     ->(r){ r.race },
        size:     ->(r){ r.size },
        need:     ->(r){ r.need },
        bike:     ->(r){ r.bike ? 'Yes' : '' },
        '' => ->(r){ link_to 'Edit', edit_family_member_path(r), remote: true, class: 'edit' if can?(:edit, r)}
      }

    when 'Organization'
      {
        name:         ->(r){ r.name },
        abbreviation: ->(r){ r.abbreviation },
        key:          ->(r){ r.key },
        email_from:   ->(r){ r.email_from },
        '' => ->(r){ link_to 'Edit', edit_organization_path(r), remote: true, class: 'edit' if can?(:edit, r)}
      }

    when 'Partnership'
      {
        program:        ->(r){ r.drive.program.name },
        drive:          ->(r){ r.drive.name },
        organization:   ->(r){ r.organization.name },
        public_link:    ->(r){  path = partner_home_path(r.organization.key, r.id)
                                link_to(path, path, remote: false, target: 'blank')
                              },
        leader:         ->(r){ r.leader.andand.name },
        active:         ->(r){ yes_no(r.active) },
        '' => ->(r){ link_to 'Edit', edit_partnership_path(r), remote: true, class: 'edit' if can?(:edit, r)}
      }

    when 'Program'
      {
        name:         ->(r){ r.name },
        abbreviation: ->(r){ r.abbreviation },
        season:       ->(r){ r.season },
        active:       ->(r){ yes_no(r.active) },
        '' => ->(r){ link_to 'Edit', edit_program_path(r), remote: true, class: 'edit' if can?(:edit, r)}
      }

    when 'Sponsor'
      {
        name:         ->(r){ r.name },
        email:        ->(r){ r.email },
        phone:        ->(r){ r.phone },
        sponsorships: ->(r){ r.sponsorships.count },
        sponsorships: ->(r){ cnt = r.sponsorships.count
                                link_to_if(cnt > 0, cnt, sponsorships_path(filter:{sponsor_id: r})) },
        '' => ->(r){ link_to 'Edit', edit_sponsor_path(r), remote: true, class: 'edit' if can?(:edit, r)}
      }

    when 'Sponsorship'
      {
        sponsor:        ->(r){ r.sponsor.name },
        syc_number:     ->(r){ r.client.andand.casenumber || r.family_member.andand.client.andand.casenumber },
        family_member:  ->(r){ r.family_member.andand.name },
        food_basket:    ->(r){ r.client.andand.name },
        created_at:     ->(r){ l(r.created_at, format: :long)},
        '' => ->(r){ link_to 'Edit', edit_sponsorship_path(r), remote: true, class: 'edit' if can?(:edit, r)}
      }

    when 'SocialWorker'
      {
        name:     ->(r){ r.name },
        email:    ->(r){ r.email },
        phone:    ->(r){ r.phone },
        '' => ->(r){ link_to 'Edit', edit_social_worker_path(r), remote: true, class: 'edit' if can?(:edit, r)}
      }

    when 'UserProfile'
      {
        name:     ->(r){ r.name },
        email:    ->(r){ r.email },
        '' => ->(r){ link_to 'Edit', edit_user_profile_path(r), remote: true, class: 'edit' if can?(:edit, r)}
      }
    else
      Hash[model_class.attribute_names.map{ |name| [name, ->(r){ r.attributes[name] }] }]
    end
  end

  def yes_no(val)
    if val === true
      t("boolean.true")
    elsif val === false
      t("boolean.false")
    end
  end

  def active_inactive(val)
    if val === true
      t("boolean.active")
    elsif val === false
      t("boolean.inactive")
    end
  end

  def number_sponsored(num_sponsored,total_size)
    if num_sponsored > 0
      phrase = (num_sponsored == total_size ? "Sponsored" : (total_size > 1 ? "#{num_sponsored} of #{total_size}" : ""))
      klass = (num_sponsored == total_size ? "sponsored" : "unsponsored")
      raw "<span class='#{klass}'>#{phrase}</span>"
    else
      ""
    end
  end

end