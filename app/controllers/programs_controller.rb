class ProgramsController < DictionariesController

  protected

  def permitted_params
    if params[:program] # can we reference this from a central place -- && can?(:manage, Program)
      params[:program].permit(:name, :abbreviation, :season, :active)
    end
  end

end