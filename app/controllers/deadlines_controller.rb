class DeadlinesController < DictionariesController

  protected

  def permitted_params
    if params[:deadline] # can we reference this from a central place -- && can?(:manage, Deadline)
      params[:deadline].permit(:drive_id, :name, :due_date, :due_time, :reminder_date, :description)
    end
  end

end