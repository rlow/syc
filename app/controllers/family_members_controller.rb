class FamilyMembersController < DictionariesController



  protected
  def permitted_params
    if params[:family_member]     # && can?(:manage, Foo)
      params[:family_member].permit(:client_id, :first_name, :age, :gender, :race, :size, :need, :bike)
    end
  end

end