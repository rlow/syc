class Public::SponsorshipController < ApplicationController
  # NOT A ResourceController!!!

  before_filter :load_common

  layout 'public'


  def new
    if (@what = params[:what]) == Sponsorship::SINGLE_FAMILY_MEMBER
      @family_member = FamilyMember.for_partnership(@partnership).where(id: params[:id]).first       # /sponsor/member/#{params[:id]}/new
      @client = @family_member.client
      @sponsorship = Sponsorship.new(family_member: @family_member)
    else
      @client = Client.for_partnership(@partnership).where(id: params[:id]).first                    # /sponsor/food/#{params[:id]}/new
      @sponsorship = Sponsorship.new(client: @client)
    end

    redirect_to "/404.html" if @client.nil?
    @sponsor = Sponsor.new
  end


  # this is a long mess - needs some serious refactoring (after Christmas)
  def create
    @what = what_params[:what]
    # set params for round trip validation errors
    @sponsorship = Sponsorship.new(sponsorship_params)

    # get the client, and family_member for potential use downstream and render new, and for redirect on success
    @client = sponsorship_params[:client_id].present? ?
                Client.where(id: sponsorship_params[:client_id]).first :
                Client.joins(:family_members).where('family_member.id' => sponsorship_params[:family_member_id]).first
    @family_member = FamilyMember.for_partnership(@partnership).where(id: sponsorship_params[:family_member_id]).first

    # first test validations on the submitted model
    @sponsor = Sponsor.new(sponsor_params)
    if @sponsor.valid?
      # now get existing sponsor or create
      @sponsor = Sponsor.where('lower(email) = ?', sponsor_params[:email].downcase).first unless sponsor_params[:email].blank?
      @sponsor = Sponsor.create(sponsor_params) unless @sponsor.present?
    end

    # TODO: if the sponsor is found, do we update the Name and Phone - or leave it?
    # TODO #2: if an existing email is supplied, but not a name, the name validation won't trigger (because no create attempted)

    taken = false
    save_count = 0

    if @sponsor.id.present?
      if @what == Sponsorship::UNSPONSORED_FAMILY_MEMBERS
        remaining = @client.family_members.reject(&:sponsored?)
        # double check for race condition
        taken = remaining.blank?

        # create a sponsorship for each of the remaining unsponsored members
        if !taken
          remaining.each do |member|
            @sponsorship = Sponsorship.new(sponsor: @sponsor, family_member: member)
            save_count += 1 if @sponsorship.save
          end
        end

      elsif @what == Sponsorship::SINGLE_FAMILY_MEMBER
        # double check for race condition
        taken = Sponsorship.where(family_member_id: sponsorship_params[:family_member_id]).present?
        if !taken
          @sponsorship = Sponsorship.new(sponsor: @sponsor, family_member_id: sponsorship_params[:family_member_id])
          save_count += 1 if @sponsorship.save
        end
      end

      # outside loop check for food basket
      if sponsorship_params[:food_basket] == "1"
        taken = Sponsorship.where(client_id: sponsorship_params[:client_id]).present?
        if !taken
          @sponsorship = Sponsorship.new(sponsor: @sponsor, client_id: sponsorship_params[:client_id])
          save_count += 1 if @sponsorship.save
        end
      end

      # will need to eventually wrap the multiple sponsor creates in a transaction
      if !taken && save_count == 0
        # TODO: test what happens if this fails
        flash.now[:error] = @sponsorship.errors.blank? ?
              "Your sponsorship was not saved. Please try again or alert the webmaster if the problem continues." :
              @sponsorship.errors.full_messages
      end
    end

    # Rails.logger.debug "======================= sponsorship_params = #{sponsorship_params}"
    # Rails.logger.debug "======================= @what = #{@what}"
    # Rails.logger.debug "======================= taken = #{taken}"
    # Rails.logger.debug "======================= save_count = #{save_count}"
    # Rails.logger.debug "======================= @sponsorship.errors.blank? = #{@sponsorship.errors.blank?}"

    if taken
      flash[:alert] = "We're sorry, but it appears that someone beat you to the punch and already sponsored this #{@what}"
      redirect_to partner_client_path(@organization.key, @partnership, @client)
    elsif save_count > 0
      # TODO test this - will need a PUT/PATCH path for sponsorship
      # make sure to re-fetch the client (and sponsor) so the new family member sponsorships are reflected
      SponsorMailer.thanks(@organization, @partnership, Client.find(@client.id), Sponsor.find(@sponsor.id)).deliver
      redirect_to partner_thanks_path(@organization.key, @partnership, @client, @sponsor)
    else
      render :new
    end
  end



  private

  def sponsorship_params
    params.require(:sponsorship).permit(:sponsor_id, :client_id, :family_member_id, :food_basket)
  end

  def sponsor_params
    params.require(:sponsor).permit(:name, :email, :phone)
  end

  def what_params
    params.permit(:what)
  end

  # TODO how to get/mixin this same code in this controller and in the partner_controller
  def load_common
    params.permit(:key, :partnership_id)

    @organization = Organization.active.where(key: params[:key]).first
    if params[:partnership_id].present?
      @partnership = Partnership.active.for_organization(@organization).where(id: params[:partnership_id]).first
    else
      @partnership = Partnership.active.for_organization(@organization).first
    end

    redirect_to "/404.html" if @partnership.nil?
  end

end