class Public::PartnerController < TopsailStarter::ResourcesController

  before_filter :load_common

  layout 'public'

  def about;  end
  def home;  end

  def index
    render action: :home if @partnerships.count == 1
  end

  def instructions;  end
  def welcome;  end

  def clients
    # default sort by SYC number
    params.merge!('sort' => 'casenumber') if params['sort'].nil?
    @records = Client.for_partnership(@partnership).includes(:family_members).
                      filter_by(params[:filter]).
                      sorted_by(params[:sort]).
                      paginate( {:page=>1}.merge(params[:paginate] || {}).symbolize_keys )
  end

  def members
    @records = FamilyMember.for_partnership(@partnership).
                     filter_by(params[:filter]).
                     sorted_by(params[:sort]).
                     paginate( {:page=>1}.merge(params[:paginate] || {}).symbolize_keys )
  end

  def bigboard
    # only get clients that are not all sponsored
    # think about looking at the last sponsorship time and letting it remain for 2-3 minutes
    all      = params[:all] && ['1', 'true'].include?(params[:all])
    @help    = params.include?(:help) && "Options: &help, &all=true|false, &columns=2" || nil
    @columns = params[:columns] && params[:columns].to_i || nil
    @clients = Client.for_partnership(@partnership).includes(:family_members).order('client.casenumber')
    @clients = @clients.reject(&:all_sponsored?) unless all
  end

  def client
    @client = Client.for_partnership(@partnership).find(params[:id]) unless @partnership.nil?
  end

  def thanks
    @client = Client.for_partnership(@partnership).where(id: params[:id]).first
    @sponsor = Sponsor.where(id: params[:sponsor_id]).first
  end

  private

  def ajaxy?
    false
  end

  # intialize @organization and @partnership or redirect
  def load_common
    @organization = Organization.active.where(key: params[:key]).first
    if params[:partnership_id].present?
      rel = Partnership.active.for_organization(@organization).where(id: params[:partnership_id])
    else
      rel = Partnership.active.for_organization(@organization)
    end
    # select available partnerships from the relation (with all upstream stuff active)
    @partnerships = rel.select(&:available)

    if @partnerships.count == 0
      # redirect_to "/404.html" if @partnership.nil?
      render action: 'welcome'
    end
    @partnership = @partnerships.first

  end

end