class SponsorsController < DictionariesController

  protected
  def permitted_params
    if params[:sponsor] # can we reference this from a central place -- && can?(:manage, Sponsor)
      params[:sponsor].permit(:name, :email, :phone)
    end
  end

end