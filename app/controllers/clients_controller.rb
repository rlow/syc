class ClientsController < DictionariesController


  def view
    @client = Client.where(id: params[:id]).first
    @partnership = @client.partnership
    @organization = @partnership.organization

    render layout: "public"
  end

  protected

  def permitted_params
    if params[:client] # can we reference this from a central place -- && can?(:manage, Client)
      params[:client].permit( :address,
                              :casenumber,
                              :city,
                              :deliverable,
                              :drive_id,
                              :first_name,
                              :last_name,
                              :latlng,
                              :partnership_id,
                              :phone,
                              :profile,
                              :social_worker_id,
                              :state,
                              :zip,
                              :notes,
                              :verified,
            { :family_members_attributes => [:id, :client_id, :first_name, :age, :gender, :race, :size, :need, :bike, :_destroy] })
    end
  end

end