class DrivesController < DictionariesController

  protected

  def permitted_params
    if params[:drive] # can we reference this from a central place -- && can?(:manage, Drive)
      params[:drive].permit(:program_id, :name, :active)
    end
  end

end