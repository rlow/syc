class DictionariesController < TopsailStarter::ResourcesController

  include Authentication # defined in topsail_starter

  layout false, except: 'index'
  
  protected
  def redirect_after_update
  end

  def redirect_after_destroy
  end

end