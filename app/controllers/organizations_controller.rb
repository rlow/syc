class OrganizationsController < DictionariesController

  protected

  def permitted_params
    if params[:organization] #can we reference this from a central place, otherwise must repeat && can?(:manage, Organization)
      params[:organization].permit( :name, :organization, :abbreviation, :key, :email_from, :active,
                                    :image, :image_url, :remove_image, :retained_image)
    end
  end

end