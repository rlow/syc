class UserProfilesController < DictionariesController

  protected
  def permitted_params
    if params[:user_profile] # can we reference this from a central place -- && can?(:manage, UserProfile)
      params[:user_profile].permit(:name, :email, :phone, :active, :permissions => [])
    end
  end

end