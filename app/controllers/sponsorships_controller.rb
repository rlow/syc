class SponsorshipsController < DictionariesController

  protected
  def permitted_params
    if params[:sponsorship] # can we reference this from a central place -- && can?(:manage, Sponsor)
      params[:sponsorship].permit(:sponsor_id, :client_id, :family_member_id)
    end
  end

end