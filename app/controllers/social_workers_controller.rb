class SocialWorkersController < DictionariesController

  protected
  def permitted_params
    if params[:social_worker] # can we reference this from a central place -- && can?(:manage, SocialWorker)
      params[:social_worker].permit(:name, :email, :phone, :active)
    end
  end

end