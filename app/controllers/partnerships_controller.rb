class PartnershipsController < DictionariesController

  protected

  def permitted_params
    if params[:partnership] # can we reference this from a central place -- && can?(:manage, Partnership)
      params[:partnership].permit(:drive_id, :organization_id, :leader_id, :instructions, :about, :home_page, :active)
    end
  end

end