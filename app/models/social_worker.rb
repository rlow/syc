class SocialWorker < ActiveRecord::Base
  
  has_many :clients
  
  validates :name, :presence => true
  validates :email, :presence => true, uniqueness: true
  
  will_sort default:    'name',
            name:       'social_worker.name',
            email:      'social_worker.email'
  
  scopes_for_boolean :active
  
end