class UserProfile < ActiveRecord::Base

  devise :database_authenticatable
  devise :lockable, :rememberable, :trackable                       # OTIONAL :timeoutable
  devise :recoverable                                               # forgot my password and used for initial password set 
  devise :registerable                                              # for user self-account password edit
  devise :validatable                                               # handles email validation
  # NOT USED :confirmable, :omniauthable
  
       
  has_many :partnerships, foreign_key: "leader_id"
  has_many :drives, through: :partnerships, foreign_key: "leader_id"
  

  validates :name, :presence => true
  validate :validate_permissions

  after_create :send_new_user_email
  
  scopes_for_boolean :active
  
  will_sort default:  :email,
            email:    'lower(user_profile.email)',
            name:     'lower(user_profile.name)'

  def permission_keys
    permissions.map(&:to_sym)
  end


  #
  # Devise
  #
  def active_for_authentication?
    self.active? ? super : false
  end
    
  def email_required?
    true
  end
  
  def password_required?
    !self.new_record?
  end


  ####################### private #######################
  private 
  
  def validate_permissions
    self.permissions.reject!(&:blank?)
    errors.add(:permissions, "includes illegal value") unless (self.permission_keys - Permission::ALL_KEYS).empty?
  end
  
  def send_new_user_email
    # from Devise recoverable.rb
    #### not yet
    # raw, enc = Devise.token_generator.generate(UserProfile, :reset_password_token)
    # self.reset_password_token = enc
    # self.reset_password_sent_at = Time.now.utc
    # self.save(:validate => false)
    # Devise::Mailer.confirmation_instructions(self, raw).deliver
  end
  
end