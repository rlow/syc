class Client < ActiveRecord::Base

  belongs_to :drive                   # can belong to just a drive until assigned to a partnership
  belongs_to :partnership
  belongs_to :social_worker

  has_many :family_members, -> {order 'age DESC'}, inverse_of: :client, dependent: :destroy

  has_one :sponsorship
  has_one :food_sponsor, through: :sponsorship, source: :sponsor

  accepts_nested_attributes_for :family_members, :allow_destroy => true

  validates :drive, :presence => true
  validates :casenumber,  :presence => true, uniqueness: { scope: :drive, message: "must be unique within a given drive" }
  validates :last_name,   :length => {:maximum => 40}, :presence => true
  validates :first_name,  :length => {:maximum => 40}
  validates :address,     :length => {:maximum => 200}
  validates :city,        :length => {:maximum => 50}
  validates :state,       :length => {:maximum => 2}
  validates :zip,         :length => {:maximum => 10}
  validates :state,       :length => {:maximum => 2}
  validates :phone,       :length => {:maximum => 100}
  validates :profile,     :length => {:maximum => 2000}

  # validate that the selected drive is the same as the partnership.drive
  validate :validate_partnership_drive

  will_sort default:        'casenumber',
            name:           'client.last_name',
            casenumber:     'client.casenumber',
            address:        'client.address',
            zip:            'client.zip'

  #  num_in_family:  ->{ aggregate_on_family_members('COUNT(1)', 'num_in_family').order('num_in_family') }
  will_filter partnership_id: ->(c){ where('client.partnership_id' => c) }

  scope :for_partnership, ->(partnership){ where('client.partnership_id' => partnership) }
  scope :for_sponsor, ->(sponsor){ joins(:sponsorship).where("sponsorship.sponsor_id" => sponsor) }
  scope :active, ->(){ joins(:partnership).where("partnership.active" => true) }

  # will_paginate setting
  self.per_page = 50

  def name
    "#{last_name}, #{first_name}"
  end

  def all_sponsored?
    food_sponsored? && family_members.count == num_sponsored
  end

  def food_sponsored?
    food_sponsor.present?
  end

  def num_family_members
    self.family_members.size
  end

  def num_sponsored
    family_members.select{|fm| fm.sponsored?}.count
  end


  private

  def validate_partnership_drive
    if partnership && drive && partnership.drive.id != drive.id
      errors.add(:base, "Partnership does not match the Drive")
    end
  end
end