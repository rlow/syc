class Ability
  include CanCan::Ability

  def initialize(user)

    permissions = user.permission_keys

    # NOTE: :manage seems to be called and overloads the :destroy 
    # Tried to replace with :edit, :destroy and the individual actions that we have defined
    can [:create, :edit, :update], Client do
      permissions.include?(:register_clients)
    end
    
    can [:create, :edit, :update], Deadline do
      permissions.include?(:setup_programs)
    end
    
    can [:create, :edit, :update], Drive do
      permissions.include?(:setup_programs)
    end
    
    can [:create, :edit, :update], FamilyMember do
      permissions.include?(:register_clients)
    end
    
    can [:create, :edit, :update], Organization do
      permissions.include?(:setup_programs)
    end
    
    can [:create, :edit, :update], Partnership do
      permissions.include?(:setup_programs)
    end
    
    can [:create, :edit, :update], Program do
      permissions.include?(:setup_programs)
    end
    
    can [:create, :edit, :update], SocialWorker do
      permissions.include?(:setup_programs)
    end
    
    can [:create, :edit, :update], Sponsor do
      permissions.include?(:register_clients)
    end
    
    can [:create, :edit, :update], UserProfile do
      permissions.include?(:setup_user_profiles)
    end
    
    
    ## Sponsorship particular
    can [:edit, :destroy], Sponsorship do
      permissions.include?(:register_clients)
    end
    
    
    #### destroy referential integrity
    can :destroy, Client do | client |
      permissions.include?(:register_clients) && !client.family_members.any?
    end
    
    can :destroy, Deadline do | deadline |
      permissions.include?(:setup_programs)
    end
    
    can :destroy, Drive do | drive |
      permissions.include?(:setup_programs) && !(drive.deadlines.any? || drive.partnerships.any?)
      
    end

    can :destroy, FamilyMember do | fm |
      permissions.include?(:register_clients)
    end

    can :destroy, Organization do | organization |
      permissions.include?(:setup_programs) && !organization.partnerships.any?
    end

    can :destroy, Partnership do | partnership |
      permissions.include?(:setup_programs) && !partnership.clients.any?
    end
    
    can :destroy, Program do | program |
      permissions.include?(:setup_programs) && !program.drives.any?
    end

    can :destroy, SocialWorker do | sw |
      permissions.include?(:setup_programs) && !sw.clients.any?
    end

    can :destroy, Sponsor do | sponsor |
      permissions.include?(:register_clients) && !sponsor.family_members.any?
    end
        
    can :destroy, UserProfile do | user |
      permissions.include?(:setup_user_profiles) && !user.drives.any?        # plus a lot more
    end


  end
end
