class Partnership < ActiveRecord::Base

  belongs_to :drive
  belongs_to :organization
  belongs_to :leader, class_name: 'UserProfile'

  has_many :clients, -> {order 'casenumber'}
  has_many :family_members, through: :clients

  validates :drive, :presence => true
  validates :organization, :presence => true
  validates :leader, :presence => true


  scope :for_organization, ->(organization){ where('partnership.organization_id' => organization) }

  scope :for_drive, ->(drive){ where('partnership.drive_id' => drive) }

  scopes_for_boolean :active

  will_sort default:      :drive,
            drive:        ->{includes(:drive).order('drive.name DESC')},
            organization: ->{includes(:organization).order('organization.name')},
            leader:       'partnership.leader'

  # our active scope trickles up to the Drive and Program
  # TODO: add Program test
  ##scope :active, ->(){ includes(:drive).where('drive.active' => true).where('partnership.active' => true) }


  # TODO validate uniqueness of drive + organization

  def name
    "#{organization.key.upcase} - #{drive.full_name}"
  end

  def short_name
    "#{organization.abbreviation} - #{drive.program.abbreviation} #{drive.name}"
  end

  # we use this to indicate if everything upstream is active -> the organization, drive, and program
  def available
    self.active && self.organization.active && self.drive.active && self.drive.program.active
  end

  def client_family_member_count
    self.clients.map(&:num_family_members).inject{|sum,x| sum + x } || 0
  end

  def percent_sponsored
    member_count = client_family_member_count
    member_count == 0 ? 0 : (((self.clients.map(&:num_sponsored).inject{|sum,x| sum + x}.to_f || 0) / member_count) * 100).to_i
  end

end