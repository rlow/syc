class Program < ActiveRecord::Base
  
  has_many :drives
  
  validates :name, :presence => true
  
  scopes_for_boolean :active

  will_sort default:    'name',
            name:       'program.name'
  
end