class Organization < ActiveRecord::Base
  
  has_many :partnerships
  has_many :drives, through: :partnerships
  has_many :clients, through: :partnerships
  has_many :leaders, through: :partnerships
  
  validates :name, :presence => true

  scopes_for_boolean :active
  
  will_sort default:    'name',
            name:       'organization.name'
  
end