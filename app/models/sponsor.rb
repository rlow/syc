class Sponsor < ActiveRecord::Base

  has_many :sponsorships

  has_many :clients, through: :sponsorships
  has_many :family_members, through: :sponsorships


  validates :name, :presence => true
  validates :email, :presence => true
  validates :email, :email => true, :unless => "email.blank?"

  will_sort default:    'name',
            name:       'sponsor.name',
            email:      'sponsor.email'

  scope :for_partnership, ->(partnership) { joins(:sponsorship).where() }

  # "this year" means a sponsorship created between March 1 and December 25 of the current year
  scope :for_this_year, ->(truefalse=true) {
    where("#{truefalse ? '' : 'NOT'} EXISTS (
      SELECT 1 FROM sponsorship
      WHERE sponsorship.sponsor_id = sponsor.id
       AND sponsorship.created_at >= ?
       AND sponsorship.created_at <= ?)",
    Date.new(Date.today.year,3,1), Date.new(Date.today.year,12,25) )
  }

  # how to do this?
  # in sponsorship, either client_id or family_member_id->client_id is in the partnership

  # scope :for_partnership, ->(partnership){ includes(:client).where('client.partnership_id' => partnership) }
  # scope :for_sponsor, ->(sponsor){ joins(:sponsorship).where("sponsorship.sponsor_id" => sponsor) }

end