class Sponsorship < ActiveRecord::Base

  belongs_to :sponsor
  belongs_to :client                          # not really the whole client but instead just the food basket
  belongs_to :family_member

  validates :sponsor, :presence => true
  validate :validate_client_or_family_member

  # how to do this?
  # scope :for_partnership, ->(partnership){ includes(:client).where('client.partnership_id' => partnership) }
  # but also needs to include family member
  #
  # working sql:
  # SELECT sponsorship.id, sponsorship.sponsor_id, sponsorship.client_id, sponsorship.family_member_id,
  #        client.id AS clid, family_member.id AS fmid, fmclient.id AS fmclientid
  # FROM sponsorship
  # LEFT JOIN client ON client.id = sponsorship.client_id
  # LEFT JOIN family_member ON family_member.id = sponsorship.family_member_id
  # LEFT JOIN client fmclient ON fmclient.id = family_member.client_id

  scopes_for_date(:created_at)

  will_filter sponsor_id:  ->(c){ where('sponsorship.sponsor_id' => c) },
              client_id:   ->(c){ where('sponsorship.client_id' => c) },
              after:       ->(t){ created_at_on_or_after(t) },
              before:      ->(t){ created_at_on_or_before(t) },
              syc_number:  ->(n){ joins("LEFT JOIN family_member ON sponsorship.family_member_id = family_member.id
                                         LEFT JOIN client c1 ON family_member.client_id = c1.id
                                         LEFT JOIN client c2 ON sponsorship.client_id = c2.id
                                         LEFT JOIN client ON client.id = c1.id OR client.id = c2.id")
                                  .where('client.casenumber' => n) },
              partnership_id: ->(p){ joins("LEFT JOIN family_member ON sponsorship.family_member_id = family_member.id
                                         LEFT JOIN client c1 ON family_member.client_id = c1.id
                                         LEFT JOIN client c2 ON sponsorship.client_id = c2.id
                                         LEFT JOIN client ON client.id = c1.id OR client.id = c2.id")
                                  .where('client.partnership_id' => p) }

  will_sort default:        :created_at,
            created_at:     'sponsorship.created_at',
            sponsor:        ->{includes(:sponsor).order('sponsor.name')},
            family_member:  ->{includes(:family_member).order('family_member.first_name')},
            syc_number:     ->{ joins("LEFT JOIN family_member ON sponsorship.family_member_id = family_member.id
                                       LEFT JOIN client c1 ON family_member.client_id = c1.id
                                       LEFT JOIN client c2 ON sponsorship.client_id = c2.id
                                       LEFT JOIN client ON client.id = c1.id OR client.id = c2.id")
                                  .order('client.casenumber') }


  # constants for what is being sponsored - these show up in the URL's so keep it simple and no spaces
  FOOD_BASKET = 'basket'
  SINGLE_FAMILY_MEMBER = 'member'
  UNSPONSORED_FAMILY_MEMBERS = 'family'

  # virtual ldap attribute
  attr_accessor :food_basket

  # virtual attribute writer
  def food_basket=(checkbox_value)
    if checkbox_value.to_i == 1       # checked
    else
    end
  end

  # reader
  def food_basket
  end


  private

  def validate_client_or_family_member
    errors.add(:sponsorship, "must have a client or family member") if client.nil? && family_member.nil?
  end

end