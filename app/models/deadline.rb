class Deadline < ActiveRecord::Base
  
  belongs_to :drive
  
  validates :drive, :presence => true
  validates :name, :presence => true
  validates :due_date, :presence => true
  
  will_sort default:    'due_date',
            due_date:   'deadline.due_date',
            name:       'deadline.name'
            
  
end