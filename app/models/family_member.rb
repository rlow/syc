class FamilyMember < ActiveRecord::Base

  belongs_to :client, inverse_of: :family_members

  has_one :sponsorship
  has_one :sponsor, through: :sponsorship, source: :sponsor

  validates :client,     presence: true
  validates :first_name, presence: true, length: {maximum: 60}
  validates :gender,     length: {maximum: 1}, inclusion: {in: %w(F M U), message: "must be F, M, or U"}, allow_blank: true
  validates :race,       length: {maximum: 1}
  validates :need,       length: {maximum: 1000}

  before_validation :uppercase_gender_and_race

  will_sort default:    :casenumber,
            casenumber: ->{includes(:client).order('client.casenumber')},
            name:       'family_member.first_name',
            gender:     'family_member.gender',
            age:        'family_member.age'

  will_filter sponsorship:    ->(v){ joins("left join sponsorship on sponsorship.family_member_id = family_member.id")
                                    .where(v == 'a' ? '1=1' : (v == 's' ? 'sponsorship.id is not null' : 'sponsorship.id is null')) },
              sponsor_id:     ->(s){ joins(:sponsorship).where('sponsorship.sponsor_id' => s) },
              partnership_id: ->(p){ for_partnership(p) }


  scope :for_partnership, ->(partnership){ includes(:client).where('client.partnership_id' => partnership) }
  scope :for_sponsor, ->(sponsor){ joins(:sponsorship).where("sponsorship.sponsor_id" => sponsor) }
  scope :active, ->(){ joins("join client on client.id = family_member.client_id")
                      .joins("join partnership on partnership.id = client.partnership_id")
                      .where("partnership.active" => true) }

  self.per_page = 50

  def name
    first_name
  end

  def age_string
    # convert fractional component to months (no decimal part will be zero but make sure it's an int
    mths = age && (age.modulo(1) * 12).round(0).to_i || 0
    yrs = age.to_i

    mths == 0 && yrs == 0 ? "" : (mths == 0 ? "#{yrs}" : (yrs == 0 ? "#{mths} months" : "#{yrs} yr, #{mths} months"))
  end

  def casenumber
    client.casenumber
  end

  def demographics
    "#{first_name} (#{gender}-#{age_string})"
  end

  def demographics_and_needs
    "#{first_name} (#{gender}-#{age_string}) #{need}"
  end

  def sponsored?
    sponsor.present?
  end

  private

  def uppercase_gender_and_race
    self.gender.upcase!
    self.race.upcase!
  end

end