class Drive < ActiveRecord::Base
  
  belongs_to :program
  
  has_many :deadlines
  has_many :partnerships
  has_many :organizations, through: :partnerships
  has_many :clients, through: :partnerships
  has_many :leaders, through: :partnerships
  
  
  validates :name, :presence => true
  validates :program, :presence => true

  scopes_for_boolean :active
  # TODO: our active scope trickles up to the Program
  # scope :active, ->(*drive){ includes(:program).where('program.active' => true).where('drive.active = ? OR drive.id IN (?)' => true, drive) }
  
  
  will_sort default:    'name',
            name:       'drive.name'
  
  
  def full_name
    "#{program.name} #{name}"
  end
  
end